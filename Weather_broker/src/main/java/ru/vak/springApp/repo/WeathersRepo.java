package ru.vak.springApp.repo;

import org.springframework.data.repository.CrudRepository;
import ru.vak.springApp.dao.Weather;

public interface WeathersRepo extends CrudRepository<Weather, Integer> {
}
