package ru.vak.springApp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.vak.springApp.dao.Weather;

import java.sql.Date;

@Controller
public class WeatherController {


    @GetMapping
    public String main(Model model) {
        Weather weather = new Weather(Date.valueOf("2020-03-15"),0);
        model.addAttribute("weather", weather);
        return "main";
    }
}
